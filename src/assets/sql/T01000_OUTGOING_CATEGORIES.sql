CREATE TABLE IF NOT EXISTS T01000_OUTGOING_CATEGORIES (
    C01000_OUTGOING_CATEGORYID INTEGER      PRIMARY KEY AUTOINCREMENT
                                            NOT NULL,
    C01000_DESCR               STRING (255) NOT NULL
                                            UNIQUE
);