export interface OutgoingItem {
    outgoingItemId: number;
    outgoingCategoryId: number;
    outgoingId: number;
    descr: string;
    amount: number;
}