export interface OutgoingCategory {
    outgoingCategoryId: number;
    descr: string;
}