export interface Outgoing {
    outgoingId: number;
    outgoingDate: Date;
}