import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { DatabaseService } from './services/database.service';
import { OutgoingsService } from './services/outgoings.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'Ausgaben',
      url: '/outgoings',
      icon: 'repeat'
    },
    {
      title: 'Auswertungen',
      url: '/evaluations',
      icon: 'pie'
    },
    {
      title: 'Budgetplanung',
      url: '/planning',
      icon: 'umbrella'
    },
    {
      title: 'Ziele',
      url: '/goals',
      icon: 'flag'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private outgoingsService: OutgoingsService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.outgoingsService.initStorage();
    });
  }
}
