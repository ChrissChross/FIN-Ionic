import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutgoingsEditPage } from './outgoings-edit.page';

describe('OutgoingsEditPage', () => {
  let component: OutgoingsEditPage;
  let fixture: ComponentFixture<OutgoingsEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutgoingsEditPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutgoingsEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
