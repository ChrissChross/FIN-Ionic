import { Component, OnInit } from '@angular/core';
import { OutgoingItem } from 'src/app/models/outgoingItem';
import { ActivatedRoute, Router } from '@angular/router';
import { DatabaseService } from 'src/app/services/database.service';
import { OutgoingItemsService } from 'src/app/services/outgoing-items.service';

@Component({
  selector: 'app-outgoings-edit',
  templateUrl: './outgoings-edit.page.html',
  styleUrls: ['./outgoings-edit.page.scss'],
})
export class OutgoingsEditPage implements OnInit {

  outgoingItem: OutgoingItem = {
    outgoingId: null,
    outgoingCategoryId: null,
    outgoingItemId: null,
    amount: null,
    descr: null
  };

  constructor(private router: Router, private route: ActivatedRoute, private outgoingItemsService: OutgoingItemsService) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    let outgoingId: number;
    let outgoingItemId: number;
    this.route.queryParams.subscribe(param => {
      outgoingId = param.outgoingIdParam;
      outgoingItemId = param.outgoingItemId;
    });
    if (outgoingId) {
      this.outgoingItem.outgoingId = outgoingId;
      if (outgoingItemId) {
        this.outgoingItem.outgoingItemId = outgoingItemId;
        this.outgoingItemsService.getOutgoingItem(outgoingItemId).then(outgoingItem => {
          this.outgoingItem = outgoingItem;
        })
      }
    }
  }

  saveOutgoingItem() {
    if (this.outgoingItem.outgoingItemId) {
      this.outgoingItemsService.updateOutgoingItem(
        this.outgoingItem.outgoingItemId, 
        this.outgoingItem.outgoingCategoryId, 
        this.outgoingItem.amount, 
        this.outgoingItem.descr
      );  
    } else {
      this.outgoingItemsService.createOutgoingItem(
        this.outgoingItem.outgoingId, 
        this.outgoingItem.outgoingCategoryId, 
        this.outgoingItem.amount, 
        this.outgoingItem.descr
      );  
    }
    this.router.navigate(['outgoings-details']);
  }

  deleteOutgoingItem() {
    this.outgoingItemsService.deleteOutgoingItem(this.outgoingItem.outgoingItemId);
    this.router.navigate(['outgoings-details']);
  }

}
