import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OutgoingsPage } from './outgoings.page';

describe('OutgoingsPage', () => {
  let component: OutgoingsPage;
  let fixture: ComponentFixture<OutgoingsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OutgoingsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OutgoingsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
