import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { OutgoingsService } from 'src/app/services/outgoings.service';
import { Outgoing } from 'src/app/models/outgoings';

@Component({
  selector: 'app-outgoings',
  templateUrl: './outgoings.page.html',
  styleUrls: ['./outgoings.page.scss'],
})
export class OutgoingsPage implements OnInit {

  outgoings: Outgoing[] = [];

  constructor(private router: Router, private outgoingsService: OutgoingsService) { }

  ngOnInit() {
    
  }

  ionViewDidEnter() {
    this.outgoingsService.getOutgoings()
      .then(outgoings => {
        this.outgoings = outgoings;
      });
  }

  goToDetailsPage(outgoingId?: number) {
    if (outgoingId) {
      let navigationExtras: NavigationExtras = {
        queryParams: {
          outgoingIdParam: outgoingId
        }
      };
      this.router.navigate(['outgoings-details'], navigationExtras);
    } else {
      this.router.navigate(['outgoings-details']);
    }
  }
}
