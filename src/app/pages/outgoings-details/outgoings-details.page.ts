import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { OutgoingsService } from 'src/app/services/outgoings.service';
import { Outgoing } from 'src/app/models/outgoings';
import { OutgoingItemsService } from 'src/app/services/outgoing-items.service';
import { OutgoingItem } from 'src/app/models/outgoingItem';

@Component({
  selector: 'app-outgoings-details',
  templateUrl: './outgoings-details.page.html',
  styleUrls: ['./outgoings-details.page.scss'],
})
export class OutgoingsDetailsPage implements OnInit {

  outgoing: Outgoing = { outgoingDate: null, outgoingId: null };
  outgoingItems: OutgoingItem[];

  constructor(private router: Router, private route: ActivatedRoute, private outgoingsService: OutgoingsService, private outgoingItemsService: OutgoingItemsService) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    let outgoingId: number;
    this.route.queryParams.subscribe(param => {
      outgoingId = param.outgoingIdParam;
    });
    if (outgoingId) {
      this.outgoingsService.getOutgoing(outgoingId).then(outgoing => {
        this.outgoing.outgoingId = outgoing.outgoingId;
        this.outgoing.outgoingDate = outgoing.outgoingDate;
        this.outgoingItemsService.getOutgoingItems(outgoingId).then(outgoingItems => {
          this.outgoingItems = outgoingItems;
        })
      });
    }
  }

  goToEditPage(outgingItemId?: number) {
    let navigationExtras: NavigationExtras;
    if (outgingItemId) {
      navigationExtras = {
        queryParams: {
          outgoingIdParam: this.outgoing.outgoingId,
          outgoingItemId: outgingItemId
        }
      };
    } else {
      navigationExtras = {
        queryParams: {
          outgoingIdParam: this.outgoing.outgoingId
        }
      };
    }
    
    this.router.navigate(['outgoings-edit'], navigationExtras);
  }

  goToOutgoingsPage() {
    this.router.navigate(['outgoings']);
  }

  saveOutgoing() {
    if (this.outgoing.outgoingId == null && this.outgoing.outgoingDate != null) {
      this.outgoingsService.createOutgoing(this.outgoing.outgoingDate);
    } else if (this.outgoing.outgoingId != null && this.outgoing.outgoingDate != null) {
      this.outgoingsService.updateOutgoing(this.outgoing.outgoingId, this.outgoing.outgoingDate);
    }    
  }

  deleteOutgoing() {
    this.outgoingsService.deleteOutgoing(this.outgoing.outgoingId);
  }
}
