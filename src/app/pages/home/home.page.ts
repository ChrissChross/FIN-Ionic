import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { OutgoingsService } from 'src/app/services/outgoings.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  cntOutgoings: number = 0;
  cntAmounts: number = 0;

  constructor(private router: Router, private outgoingsService: OutgoingsService) {}

  ionViewWillEnter() {
    this.outgoingsService.countOutgoings().then(res => {
      this.cntOutgoings = res;
    });
    this.outgoingsService.countOutgoingAmounts().then(res => {
      this.cntAmounts = res;
    });
  }

  ionViewDidEnter() {

  }

  goToPage(path: string) {
    this.router.navigate([path]);
  }
}
