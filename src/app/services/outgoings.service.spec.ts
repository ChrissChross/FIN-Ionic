import { TestBed } from '@angular/core/testing';

import { OutgoingsService } from './outgoings.service';

describe('OutgoingsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OutgoingsService = TestBed.get(OutgoingsService);
    expect(service).toBeTruthy();
  });
});
