import { TestBed } from '@angular/core/testing';

import { OutgoingCategoriesService } from './outgoing-categories.service';

describe('OutgoingCategoriesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OutgoingCategoriesService = TestBed.get(OutgoingCategoriesService);
    expect(service).toBeTruthy();
  });
});
