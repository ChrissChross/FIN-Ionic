import { Injectable } from '@angular/core';
import { DatabaseService } from './database.service';
import { Outgoing } from "../models/outgoings";

@Injectable({
  providedIn: 'root'
})
export class OutgoingsService {

  constructor(private databaseService: DatabaseService) { }

  initStorage(): Promise<any> {
    return this.databaseService.initDatabase();
  }

  getOutgoings(): Promise<Outgoing[]> {
    let sql = `
      SELECT C02000_OUTGOINGID AS OUTGOINGID, 
        DATE(C02000_OUTGOING_DATE) AS OUTGOING_DATE 
      FROM T02000_OUTGOINGS 
      ORDER BY C02000_OUTGOING_DATE DESC;
    `;
    return this.databaseService.getDb().executeSql(sql, [])
      .then(res => {
        let results: Outgoing[] = [];
        for (let i = 0; i < res.rows.length; i++) {
          let outgoing: Outgoing = {
            outgoingId: res.rows.item(i).OUTGOINGID,
            outgoingDate: res.rows.item(i).OUTGOING_DATE
          };
          results.push(outgoing);
        }
        return results;
      });
  }

  getOutgoing(outgoingId: number): Promise<Outgoing> {
    let sql = `
      SELECT C02000_OUTGOINGID AS OUTGOINGID, 
        C02000_OUTGOING_DATE AS OUTGOING_DATE 
      FROM T02000_OUTGOINGS 
      WHERE C02000_OUTGOINGID = ?1;
    `;
    return this.databaseService.getDb().executeSql(sql, [outgoingId]).then(res => {
      let results: Outgoing = {
        outgoingId: res.rows.item(0).OUTGOINGID,
        outgoingDate: res.rows.item(0).OUTGOING_DATE
      };
      return results;
    });
  } 
  
  createOutgoing(date: Date) {
    let sql = `
      INSERT INTO T02000_OUTGOINGS (C02000_OUTGOING_DATE) 
      VALUES (?1);
    `;
    this.databaseService.getDb().executeSql(sql, [date])
      .then(res => console.log(res));
  }

  updateOutgoing(outgoingId: number, date: Date) {
    let sql = `
      UPDATE T02000_OUTGOINGS SET 
        C02000_OUTGOING_DATE = ?1 
      WHERE C02000_OUTGOINGID = ?2;
    `;
    this.databaseService.getDb().executeSql(sql, [date, outgoingId])
      .then(res => console.log(res));
  }

  deleteOutgoing(outgoingId: number) {
    let sql = `
      DELETE FROM T02000_OUTGOINGS
      WHERE C02000_OUTGOINGID = ?1;
    `;
    this.databaseService.getDb().executeSql(sql, [outgoingId]);
  }
  
  countOutgoings(): Promise<number> {
    let sql = `
      SELECT COUNT(C02000_OUTGOINGID) AS CNT_OUTGOINGS
      FROM T02000_OUTGOINGS;
    `;
    return this.databaseService.getDb().executeSql(sql, []).then(res => {
      let result: number;
      result = res.rows.item(0).CNT_OUTGOINGS;
      return result;
    })
    .catch(err => {
      console.error(err);
      return 0;
    });
  }

  countOutgoingAmounts() {
    let sql = `
      SELECT SUM(C02010_AMOUNT) AS CNT_AMOUNT
      FROM T02000_OUTGOINGS
      JOIN T02010_OUTGOING_ITEMS
        ON C02000_OUTGOINGID = C02010_OUTGOINGID;
    `;
    return this.databaseService.getDb().executeSql(sql, []).then(res => {
      let result: number;
      result = res.rows.item(0).CNT_AMOUNT;
      return result;
    })
    .catch(err => {
      console.error(err);
      return 0;
    });
  }
}
