import { Injectable } from '@angular/core';
import { DatabaseService } from './database.service';
import { OutgoingCategory } from '../models/outgoingCategory';

@Injectable({
  providedIn: 'root'
})
export class OutgoingCategoriesService {

  constructor(private databaseService: DatabaseService) { }

  getOutgoingCategories(): Promise<OutgoingCategory[]> {
    let sql = `
      SELECT C01000_OUTGOING_CATEGORYID AS OUTGOING_CATEGORYID, 
        C01000_DESCR AS DESCR
      FROM T01000_OUTGOING_CATEGORIES;
    `;
    return this.databaseService.getDb().executeSql(sql, []).then(res => {
      let results: OutgoingCategory[] = [];
      for (let i = 0; i < res.rows.length; i++) {
        results.push((res.rows.item(i)));
      }
      return results;
    });
  }

  getOutgoingCategory(outgoingCategoryId: number): Promise<OutgoingCategory> {
    let sql = `
      SELECT C01000_OUTGOING_CATEGORYID AS OUTGOING_CATEGORYID,
        C01000_DESCR AS DESCR
      FROM T01000_OUTGOING_CATEGORIES
      WHERE C01000_OUTGOING_CATEGORYID = ?1;
    `;
    return this.databaseService.getDb().executeSql(sql, [outgoingCategoryId]).then(res => {
      let result: OutgoingCategory = {
        outgoingCategoryId: res.rows.item(0).OUTGOING_CATEGORYID,
        descr: res.rows.item(0).DESCR
      };
      return result;
    })
  }

  createOutgoingCategory(descr: string) {
    let sql = `
      INSERT INTO T01000_OUTGOING_CATEGORIES (C01000_DESCR)
      VALUES (?1);
    `;
    this.databaseService.getDb().executeSql(sql, [descr]);
  }

  updateOutgoingCategory(outgoingCategoryId: number, descr: string) {
    let sql = `
      UPDATE T01000_OUTGOING_CATEGORIES SET
        C01000_DESCR = ?1
      WHERE C01000_OUTGOING_CATEGORYID = ?2;
    `;
    this.databaseService.getDb().executeSql(sql, [descr, outgoingCategoryId]);
  }

  deleteOutgoingCategory(outgoingCategoryId: number) {
    let sql = `
      DELETE FROM T01000_OUTGOING_CATEGORIES
      WHERE C01000_OUTGOING_CATEGORYID = ?1;
    `;
    this.databaseService.getDb().executeSql(sql, [outgoingCategoryId]);
  }
}
