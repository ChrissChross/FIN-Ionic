import { Injectable } from '@angular/core';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';
import { HttpClient } from '@angular/common/http';

const win: any = window;

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  private db: SQLiteObject;

  constructor(private sqllite: SQLite, private http: HttpClient) { }

  initDatabase() {
    if (win.sqlitePlugin) {
      return this.sqllite.create({
        name: 'fin.db', 
        location: 'default'
      }).then(db => {
        this.db = db;
        this.http.get('assets/sql/T01000_OUTGOING_CATEGORIES.sql', { responseType: 'text' })
          .subscribe(sql => {
            this.db.executeSql(sql, [])
              .then(res => console.log('CREATED TABLE T01000_OUTGOING_CATEGORIES'))
              .catch(err => console.log(err));
          });
        this.http.get('assets/sql/T02000_OUTGOINGS.sql', { responseType: 'text' })
        .subscribe(sql => {
          this.db.executeSql(sql, [])
            .then(res => console.log('CREATED TABLE T02000_OUTGOINGS'))
            .catch(err => console.log(err));
          });
        this.http.get('assets/sql/T02010_OUTGOING_ITEMS.sql', { responseType: 'text' })
        .subscribe(sql => {
          this.db.executeSql(sql, [])
            .then(res => console.log('CREATED TABLE T02010_OUTGOING_ITEMS'))
            .catch(err => console.log(err));
          });
        });
    } else {
      console.log('sqlite does not exist');
    }
  }

  getDb(): SQLiteObject {
    return this.db;
  }
}
