import { TestBed } from '@angular/core/testing';

import { OutgoingItemsService } from './outgoing-items.service';

describe('OutgoingItemsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OutgoingItemsService = TestBed.get(OutgoingItemsService);
    expect(service).toBeTruthy();
  });
});
