import { Injectable } from '@angular/core';
import { DatabaseService } from './database.service';
import { OutgoingItem } from '../models/outgoingItem';

@Injectable({
  providedIn: 'root'
})
export class OutgoingItemsService {

  constructor(private databaseService: DatabaseService) { }

  getOutgoingItems(outgoingId: number): Promise<OutgoingItem[]> {
    let sql = `
      SELECT C02010_OUTGOING_ITEMID AS OUTGOING_ITEMID, 
        C02010_OUTGOING_CATEGORYID AS OUTGOING_CATEGORYID, 
        C02010_OUTGOINGID AS OUTGOINGID, 
        C02010_DESCR AS DESCR, 
        C02010_AMOUNT AS AMOUNT
      FROM T02010_OUTGOING_ITEMS 
      WHERE C02010_OUTGOINGID = ?1;
    `;
    return this.databaseService.getDb().executeSql(sql, [outgoingId]).then(res => {
      let results: OutgoingItem[] = [];
      for (let i = 0; i < res.rows.length; i++) {
        let outgoingItem: OutgoingItem = {
          outgoingItemId: res.rows.item(i).OUTGOING_ITEMID,
          outgoingCategoryId: res.rows.item(i).OUTGOING_CATEGORYID,
          outgoingId: res.rows.item(i).OUTGOINGID,
          amount: res.rows.item(i).AMOUNT,
          descr: res.rows.item(i).DESCR
        };
        results.push(outgoingItem);
      }
      return results;
    });
  }

  getOutgoingItem(outgoingItemId: number): Promise<OutgoingItem> {
    let sql = `
      SELECT C02010_OUTGOING_ITEMID AS OUTGOING_ITEMID, 
        C02010_OUTGOING_CATEGORYID AS OUTGOING_CATEGORYID, 
        C02010_OUTGOINGID AS OUTGOINGID, 
        C02010_DESCR AS DESCR, 
        C02010_AMOUNT AS AMOUNT
      FROM T02010_OUTGOING_ITEMS 
      WHERE C02010_OUTGOING_ITEMID = ?1;`;
    return this.databaseService.getDb().executeSql(sql, [outgoingItemId]).then(res => {
      let result: OutgoingItem = {
        outgoingId: res.rows.item(0).OUTGOINGID,
        outgoingItemId: res.rows.item(0).OUTGOING_ITEMID,
        outgoingCategoryId: res.rows.item(0).OUTGOING_CATEGORYID,
        amount: res.rows.item(0).AMOUNT,
        descr: res.rows.item(0).DESCR
      };
      return result;
    })
  }

  createOutgoingItem(outgoingId: number, outgoingCategoryId: number, amount: number, descr: string) {
    let sql = `
      INSERT INTO T02010_OUTGOING_ITEMS (C02010_OUTGOINGID, C02010_OUTGOING_CATEGORYID, C02010_AMOUNT, C02010_DESCR) 
      VALUES (?1, ?2, ?3, ?4);
    `;
    this.databaseService.getDb().executeSql(sql, [outgoingId, outgoingCategoryId, amount, descr]);
  }

  updateOutgoingItem(outgoingItemId: number, outgoingCategoryId: number, amount: number, descr: string) {
    let sql = `
      UPDATE T02010_OUTGOING_ITEMS SET 
        C02010_OUTGOING_CATEGORYID = ?1,
        C02010_AMOUNT = ?2,
        C02010_DESCR = ?3
      WHERE C02010_OUTGOING_ITEMID = ?4;
    `;
    this.databaseService.getDb().executeSql(sql, [outgoingCategoryId, amount, descr, outgoingItemId]);
  }

  deleteOutgoingItem(outgoingItemId: number) {
    let sql = `
      DELETE FROM T02010_OUTGOING_ITEMS 
      WHERE C02010_OUTGOING_ITEMID = ?1;
    `;
    this.databaseService.getDb().executeSql(sql, [outgoingItemId]);
  }
}
