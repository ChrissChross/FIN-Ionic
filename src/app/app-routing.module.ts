import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './pages//home/home.module#HomePageModule'
  },
  { path: 'outgoings', loadChildren: './pages/outgoings/outgoings.module#OutgoingsPageModule' },
  { path: 'evaluations', loadChildren: './pages/evaluations/evaluations.module#EvaluationsPageModule' },
  { path: 'goals', loadChildren: './pages/goals/goals.module#GoalsPageModule' },
  { path: 'planning', loadChildren: './pages/planning/planning.module#PlanningPageModule' },
  { path: 'outgoings-details', loadChildren: './pages/outgoings-details/outgoings-details.module#OutgoingsDetailsPageModule' },
  { path: 'outgoings-edit', loadChildren: './pages/outgoings-edit/outgoings-edit.module#OutgoingsEditPageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
